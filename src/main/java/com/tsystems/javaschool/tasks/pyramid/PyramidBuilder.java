package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        int[][] ourMass = checkIfPossible(inputNumbers);
        Collections.sort(inputNumbers);

        // fill in values MAIN CODE
        int count = 0;                 // counter of variables from List
        int countRow = 1;              // the row we stay at
        int center = ourMass[0].length / 2; // center
        int temp2 = center;
        for (int i = 0; i < ourMass.length; i++) {
            if (i == 0) {
                ourMass[i][center] = inputNumbers.get(count);
                count++;
                center--;
                countRow++;
            } else {

                int temp = countRow;
                while (temp > 0) {
                    ourMass[i][center] = inputNumbers.get(count);
                    count++;
                    temp--;
                    center += 2;
                    if (center > ourMass[i].length) center = ourMass[i].length;
                }
                center = temp2 - countRow;
                if (center < 0) center = 0;
                countRow++;
            }
        }
        return ourMass;
    }

    private static int[][] checkIfPossible(List input) {
         int size = input.size();
         if (size > Integer.MAX_VALUE-2) {
            throw new CannotBuildPyramidException();
         }

        // check if there is any null in the list
        for (int i = 0; i < size; i++) {
            if (input.get(i) == null) {
                throw new CannotBuildPyramidException();
            }
        }

        int columns = 1;
        int rows = 1;
        int quantity = 0;
        while (quantity < size) {
            quantity = quantity + rows;
            rows++;
            columns = columns + 2;
        }

        //Check if possible to build the pyramid
        if (quantity != size) {
            throw new CannotBuildPyramidException();
        }

        rows = rows - 1;
        columns = columns - 2;

        // result
        int[][] result = new int[rows][columns];

        // filling in with zero
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                result[i][j] = 0;
            }
        }
        return result;
    }
}
