package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        if (statement==null || statement.equals("") || statement.matches(".*(\\D)(\\1+).*"))
        {
            return null;
        }

        ExpressionParser n = new ExpressionParser();
        List<String> expression = n.parse(statement);
        boolean flag = n.flag;
        if (flag) {
            DecimalFormat df = new DecimalFormat("#.####");
            df.setRoundingMode(RoundingMode.FLOOR);
            Result result = new Result();
            if (result.calc(expression)==null) {
                return null;
            }
            Double d = result.calc(expression).doubleValue();

            if(d % 1 == 0.0) {
                return String.valueOf(d.intValue());
            } else {
                return d.toString();
            }
        }
        return null;
    }

    class ExpressionParser {
        private String operators = "+-*/";
        private String delimiters = "() " + operators;
        public boolean flag = true;
        private boolean isDelimiter(String token) {
            if (token.length() != 1) return false;
            for (int i = 0; i < delimiters.length(); i++) {
                if (token.charAt(0) == delimiters.charAt(i)) return true;
            }
            return false;
        }

        private boolean isOperator(String token) {
            if (token.equals("u-")) return true;
            for (int i = 0; i < operators.length(); i++) {
                if (token.charAt(0) == operators.charAt(i)) return true;
            }
            return false;
        }

        private int priority(String token) {
            if (token.equals("(")) return 1;
            if (token.equals("+") || token.equals("-")) return 2;
            if (token.equals("*") || token.equals("/")) return 3;
            return 4;
        }

        public List<String> parse(String infix) {
            List<String> postfix = new ArrayList<String>();
            Deque<String> stack = new ArrayDeque<String>();
            StringTokenizer tokenizer = new StringTokenizer(infix, delimiters, true);
            String prev = "";
            String curr = "";
            while (tokenizer.hasMoreTokens()) {
                curr = tokenizer.nextToken();
                if(curr.contains(",")) {
                    flag = false;
                    return postfix;
                }
                if (!tokenizer.hasMoreTokens() && isOperator(curr)) {
                    flag = false;
                    return postfix;
                }
                if (curr.equals(" ")) continue;
                else if (isDelimiter(curr)) {
                    if (curr.equals("(")) stack.push(curr);
                    else if (curr.equals(")")) {
                        while (!stack.peek().equals("(")) {
                            postfix.add(stack.pop());
                            if (stack.isEmpty()) {
                                flag = false;
                                return postfix;
                            }
                        }
                        stack.pop();
                        if (!stack.isEmpty()) {
                            postfix.add(stack.pop());
                        }
                    }
                    else {
                        if (curr.equals("-") && (prev.equals("") || (isDelimiter(prev)  && !prev.equals(")")))) {
                            curr = "u-";
                        }
                        else {
                            while (!stack.isEmpty() && (priority(curr) <= priority(stack.peek()))) {
                                postfix.add(stack.pop());
                            }

                        }
                        stack.push(curr);
                    }

                }

                else {
                    postfix.add(curr);
                }
                prev = curr;
            }

            while (!stack.isEmpty()) {
                if (isOperator(stack.peek())) postfix.add(stack.pop());
                else {
                    flag = false;
                    return postfix;
                }
            }
            return postfix;
        }
    }

    class Result {
        public Double calc(List<String> postfix) {
            Deque<Double> stack = new ArrayDeque<Double>();
            for (String x : postfix) {
                if (x.equals("+")) stack.push(stack.pop() + stack.pop());
                else if (x.equals("-")) {
                    Double b = stack.pop(), a = stack.pop();
                    stack.push(a - b);
                }
                else if (x.equals("*")) stack.push(stack.pop() * stack.pop());
                else if (x.equals("/")) {
                    Double b = stack.pop(), a = stack.pop();
                    if (b.equals(0.0)) {
                        return null;
                    }

                    stack.push(a / b);
                }
                else if (x.equals("u-")) stack.push(-stack.pop());
                else stack.push(Double.valueOf(x));
            }
            return stack.pop();
        }
    }
}


