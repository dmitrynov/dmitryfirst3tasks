package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        else if (x.size() > y.size()) {
            return false;
        }
        else if (x.isEmpty()) {
            return true;
        }

        boolean flag = false;
        int position = 0;
        int countLength = 0;

        for (int i = 0; i < x.size(); i++) {
            // check each element in y list
            for (int j = position; j < y.size(); j++) {
                if (x.get(i).equals(y.get(j))) {
                    position = j;
                    position++;
                    countLength++;
                    flag = true;
                    break;
                }
            }
        }

        if (flag == true && countLength == x.size()) {
            return true;
        }
        return false;
    }
}
